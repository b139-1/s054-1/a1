let collection = [];

// Write the queue functions below.

const print = () => {
	return collection;
}

const enqueue = (element) => {
	collection[collection.length] = element
	return collection

}

const dequeue = () => {
	collection.splice(0, 1);
    if (collection.length > 0) {
        return collection;
    }
	
}

const front = () => {
	return collection[0]
}

const size = () => {
	return collection.length
}

 const isEmpty = () => {
 	if(collection.length <= 0) {
 		return true
 	} else {
 		return false
 	}
 }



module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};